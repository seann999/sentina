import cPickle as pickle
import numpy as np

try:
    data, name_target, num_target, name2num, num2name = pickle.load(open("data.p", "rb"))
except:
    print("pickle not found")

data = np.asarray(data)
target = num_target

target = np.asarray(target)
target = np.reshape(target, (target.shape[0], 1))

np.random.seed(1)
n_data = 100000
noise = np.random.randn(n_data, 128)
noise /= noise.sum(axis=1)[:, np.newaxis]
data = np.vstack((data, noise))
target = np.vstack((target, np.reshape([max(target) + 1] * n_data, (n_data, 1))))

target -= 1 #1-index to 0-index

from sklearn.neural_network import MLPClassifier
clf = MLPClassifier(algorithm='adam', hidden_layer_sizes=(128,), random_state=1, max_iter=1000)

print(data.shape)
print(target.shape)

clf.fit(data, target)

predict = clf.predict(data[:-n_data])

print(target.squeeze())
print(predict)
correct = np.equal(predict, target.squeeze()[:-n_data])
print(correct)
print(np.mean(correct))

pickle.dump(clf, open("classifier.p", "wb"))