import openface
import cv2
import cPickle as pickle
import sys
# `args` are parsed command-line arguments.

imgDim = 96

align = openface.AlignDlib(sys.argv[1] + "/models/dlib/shape_predictor_68_face_landmarks.dat")
#net = openface.TorchNeuralNet(model="/home/sean/openface/models/openface/nn4.small2.v1.t7", imgDim=imgDim)#, cuda=args.cuda)
net = openface.TorchNeuralNet(model="nn4.small2.v1.ascii.t7", imgDim=imgDim)

cap = cv2.VideoCapture(int(sys.argv[2]))
cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160);
cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120);

rep1 = None
data = []
target = []
name_target = []
index = 0

try:
    data, name_target, num_target, name2num, num2name = pickle.load(open("data.p", "rb"))
except:
    print("pickle not found")

done = False
name = raw_input("enter username: ")#name of face?")

while not done:
    while(True):
        userdata = []
        # Capture frame-by-frame
        ret, frame = cap.read()

        if not ret:
            continue

        #print("frame: %s" % (frame.shape,))

        img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # `img` is a numpy matrix containing the RGB pixels of the image.
        bb = align.getLargestFaceBoundingBox(img)

        #print("image: %s" % bb)

        alignedFace = align.align_v1(imgDim, img, bb,
                                     landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if alignedFace is not None:
            #print(bb)
            #print("aligned face: %s" % (alignedFace.shape,))
            #Image.fromarray(alignedFace, 'RGB').show()

            #if rep1 is None:
            rep1 = net.forward(alignedFace)
            #userdata.append(rep1)
            data.append(rep1)
            target.append(index)
            name_target.append(name)

            # Our operations on the frame come here

            # Display the resulting frame
            cv2.rectangle(frame, (bb.left(), bb.top()), (bb.right(), bb.bottom()), (0, 255, 0), thickness=4)

        else:
            print("aligned face is None")

        cv2.imshow('face', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    #data.append(userdata)
    index += 1
    name = raw_input("press q to finish, or enter next username: ")

    if name == "q":
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

#print(data)
print(name_target)

from collections import defaultdict
from itertools import count
from functools import partial

name2num = defaultdict(partial(next, count(1)))
num_target = [name2num[label] for label in name_target]

print (name2num)

num2name = {v: k for k, v in name2num.items()}
num2name = [num2name[i + 1] for i in range(len(name2num.keys()))]

print(num_target)

pickle.dump([data, name_target, num_target, name2num, num2name], open("data.p", "wb"))
