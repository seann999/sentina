import openface
import cv2
import cPickle as pickle
import numpy as np
from PIL import Image
#from sklearn.manifold import TSNE
#import matplotlib.pyplot as plt
import time
import sys
import thread

try:
    import motor
except:
    pass

# `args` are parsed command-line arguments.

imgDim = 96
successes = 0.0
open_door = False
last_open = 0
success_requirement = 2

openface_path = sys.argv[1]
align = openface.AlignDlib(openface_path + "/models/dlib/shape_predictor_68_face_landmarks.dat")
net = openface.TorchNeuralNet(model="nn4.small2.v1.ascii.t7", imgDim=imgDim)#, cuda=args.cuda)

cap = cv2.VideoCapture(int(sys.argv[2]))
cv2.namedWindow("sentina", cv2.WND_PROP_OPENGL)          
cv2.setWindowProperty("sentina", cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)

try:
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH,160);
    cap.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT,120);
except:
    cap.set(cv2.CAP_PROP_FRAME_WIDTH,160);
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT,120);

rep1 = None
data = []
target = []
name_target = []
index = 0
quit = False
frame = None
alignedFace = None
label_updated = False

current_confidence = -1
current_box = None
current_name = None

not_recognized_text = "not recognized"

try:
    data, name_target, num_target, name2num, num2name = pickle.load(open("data.p", "rb"))
    clf = pickle.load(open("classifier.p", "rb"))
except:
    print("pickle not found")
    sys.exit(0)

def equalize(mat):
    equal = cv2.cvtColor(mat, cv2.COLOR_BGR2YCR_CB)
    y,cr,cb = cv2.split(equal)
    clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(8, 8))
    y = clahe.apply(y)
    equal = cv2.merge((y,cr,cb))
    return cv2.cvtColor(equal, cv2.COLOR_YCR_CB2BGR)

def start_display():
    global frame

    while (True):
        ret, frame = cap.read()

        if not ret:
            continue

        frame = equalize(frame)

        if alignedFace is not None:
            col = (0, 0, 0)

            if current_name is None:
                text = "???"
            else:
                if label_updated:
                    text = ""
                else:
                    text = "*"

                text += "%s: %i%%" % (current_name, current_confidence)

                if current_name == not_recognized_text:
                    col = (0, 0, 255)

                if current_confidence >= 90:
                    col = (255, 255, 0)
                elif current_confidence <= 80:
                    col = (0, 128, 255)
                elif current_confidence <= 90:
                    col = (0, 255, 255)

            if current_box is not None:
                cv2.rectangle(frame, (current_box[0], current_box[1]), (current_box[2], current_box[3]), col, thickness=1)
                cv2.putText(frame, text, (current_box[0], current_box[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, col, thickness=1)

        resized_frame = cv2.resize(frame, None, fx=2, fy=2, interpolation=cv2.INTER_LINEAR)

        cv2.imshow('sentina', resized_frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            global quit
            quit = True
            break

    cap.release()
    cv2.destroyAllWindows()
    sys.exit(0)

def overlap(box1, box2):
    x5 = max(box1[0], box2[0])
    y5 = max(box1[3], box2[3])
    x6 = min(box1[2], box2[2])
    y6 = min(box1[1], box2[1])

    intersection = float(abs(x5 - x6)*abs(y5 - y6))
    orig = float(abs(box1[0] - box1[2]) * abs(box1[1] - box1[3]))
    ratio = intersection / orig

    #print(ratio)

    return ratio > 0.8

def start_recognition():
    global current_name, col, current_confidence, label_updated, successes

    while (True):
        time.sleep(0.2)

        if alignedFace is not None:
            rep1 = net.forward(alignedFace)

            predict_num = clf.predict(rep1.reshape(1, 128))[0]
            logits = clf.decision_function(rep1.reshape(1, 128))
            softmax = np.exp(logits) / np.sum(np.exp(logits))

            if predict_num == len(num2name):
                name = not_recognized_text
            else:
                name = num2name[predict_num]

       	    current_confidence = int(np.max(softmax) * 100.0)

            current_name = name
            label_updated = True

            #print("recognized: %s" % name)

	    if name != not_recognized_text and current_confidence >= 90:
                successes += 1
            else:
                successes = 0

	    print(successes)

            if successes >= success_requirement:
                global open_door
                print("SUCCESS")
                open_door = True
                successes = 0
        else:
            pass
            #successes = 0
            #print("not recognized")
            #current_name = None

def start_detection():
    global alignedFace, label_updated, current_box, current_name

    while(True):
        time.sleep(0.1)

        if frame is None:
            continue
        else:
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        bb = align.getLargestFaceBoundingBox(img)

        alignedFace = align.align_v1(imgDim, img, bb,
                                     landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if alignedFace is not None:
            new_box = [bb.left(), bb.top(), bb.right(), bb.bottom()]
            current_box = new_box
            label_updated = False
        else:
            current_box = None

if __name__ == "__main__":
    thread.start_new_thread(start_display, ())
    thread.start_new_thread(start_detection, ())
    thread.start_new_thread(start_recognition, ())

    while 1:
        if open_door and (time.time() - last_open) >= 10:
            open_door = False

            try:
                motor.trigger()
                last_open = time.time()
            except:
                pass

            print("open sesame")
        if quit:
            sys.exit(0)
	time.sleep(0.1)
