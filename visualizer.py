import cPickle as pickle
import numpy as np
from sklearn.manifold import TSNE

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import time

name_target = None

try:
    data, name_target, num_target, name2num, num2name = pickle.load(open("data.p", "rb"))
except:
    print("pickle not found")

data = np.asarray(data)
print(data.shape)

target = np.asarray(num_target)
target = np.reshape(target, (target.shape[0], 1))

show_noise = False

if show_noise:
    np.random.seed(1)
    n_data = 100
    noise = np.random.randn(n_data, 128)
    noise /= noise.sum(axis=1)[:, np.newaxis]
    data = np.vstack((data, noise))
    target = np.vstack((target, np.reshape([max(target) + 1] * n_data, (n_data, 1))))
    num2name.append("noise")

X_tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000).fit_transform(data)
fig = plt.figure(figsize=(10, 5))
print(target)

scatters = []
colors = iter(cm.rainbow(np.linspace(0, 1, max(target))))

for i in range(max(target)):
    indices = np.where(target == (i + 1))
    print(indices)
    s = plt.scatter(X_tsne[indices, 0], X_tsne[indices, 1], c=next(colors))
    scatters.append(s)

print(name_target)
print(num2name)
print(name2num)

plt.legend(scatters, num2name)
fig.savefig("graphs/%s.png" % time.time())
