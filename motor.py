#!/usr/bin/python
# coding: utf-8 

import RPi.GPIO as GPIO
import time
import signal
import sys

def to_dc(x):
	return float(x)/10.0 + 2.5#(1.0 + x/180.0)/20.0*100.0 # calculate duty

def exit_handler(signal, frame):
        # Ctrl+Cが押されたときにデバイスを初期状態に戻して終了する。
        print("\nExit")
        servo.ChangeDutyCycle(2.0)
        time.sleep(0.5)
        servo.stop()
        GPIO.cleanup()
        sys.exit(0)

#servo.ChangeDutyCycle(to_dc(start_ang))

def trigger():
	# GPIO 21番を使用
	gp_out = 18
	GPIO.setmode(GPIO.BCM)

	GPIO.setup(gp_out, GPIO.OUT)
	# pwm = GPIO.PWM([チャンネル], [周波数(Hz)])
	servo = GPIO.PWM(gp_out, 100)
	servo.start(5)

	start_ang = 150
	end_ang = 70
        
# 終了処理用のシグナルハンドラを準備
         
        for angle in range(start_ang, end_ang, end_ang - start_ang):
                dc =to_dc(angle)#(1.0 + angle/180.0)/20.0*100.0 # calculate duty
                servo.ChangeDutyCycle(dc)
                print(">> Angle = %d" % angle)
                print("dc = %d" % dc)
                time.sleep(0.5)
        for angle in range(end_ang, start_ang + 1, start_ang - end_ang):
		dc =to_dc(angle)#.0 + angle/180.0)/20.0*100.0 # calculate duty
                servo.ChangeDutyCycle(dc)
                print("<< Angle = %d" % angle)
                print("dc = %d" % dc)
                time.sleep(0.5)

	signal.signal(signal.SIGINT, exit_handler)

