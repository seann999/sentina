import openface
import cv2
import cPickle as pickle
import numpy as np
from PIL import Image
import sys
# `args` are parsed command-line arguments.
import thread
imgDim = 96

openface_path = sys.argv[1]
align = openface.AlignDlib(openface_path + "/models/dlib/shape_predictor_68_face_landmarks.dat")
net = openface.TorchNeuralNet(model="nn4.small2.v1.ascii.t7", imgDim=imgDim)#, cuda=args.cuda)

cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH,160);
cap.set(cv2.CAP_PROP_FRAME_HEIGHT,120);

rep1 = None
data = []
target = []
name_target = []
index = 0
frame = None
update_counter = 0
current_box = None
current_name = None
col = (0, 0, 0)

try:
    data, name_target, num_target, name2num, num2name = pickle.load(open("data.p", "rb"))
    clf = pickle.load(open("classifier.p", "rb"))
except:
    print("pickle not found")
    sys.exit(0)

def equalize(mat):
    equal = cv2.cvtColor(mat, cv2.COLOR_BGR2YCR_CB)
    y,cr,cb = cv2.split(equal)
    clahe = cv2.createCLAHE(clipLimit=1.0, tileGridSize=(8, 8))
    y = clahe.apply(y)
    equal = cv2.merge((y,cr,cb))
    return cv2.cvtColor(equal, cv2.COLOR_YCR_CB2BGR)

def start_display(mt=False):
    global frame
    index = 0
    while (True):
        print("capture")
        ret, frame = cap.read()

        if not ret:
            continue

        frame = equalize(frame)
        #img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        #if not mt:
        #if index % 3 == 0:
        start_detection(loop=False)

        #index += 1
        #      thread.start_new_thread(start_detection, (False,))

        #print(current_box)
        #print(current_name)

        if current_box is not None:
            cv2.rectangle(frame, (current_box[0], current_box[1]), (current_box[2], current_box[3]), col, thickness=4)
            cv2.putText(frame, current_name, (current_box[1], current_box[0]), cv2.FONT_HERSHEY_SIMPLEX, 1, col, thickness=3)

        resized_frame = cv2.resize(frame, None, fx=4, fy=4, interpolation=cv2.INTER_LINEAR)

        print("draw")
        cv2.imshow('face', resized_frame)
        #time.sleep(0.1)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()

def overlap(box1, box2):
    x5 = max(box1[0], box2[0])
    y5 = max(box1[3], box2[3])
    x6 = min(box1[2], box2[2])
    y6 = min(box1[1], box2[1])

    intersection = float(abs(x5 - x6)*abs(y5 - y6))
    orig = float(abs(box1[0] - box1[2]) * abs(box1[1] - box1[3]))
    ratio = intersection / orig

    #print(ratio)

    return ratio > 0.8

def start_detection(loop=False):
    def detect():
        if frame is not None:
            img = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        bb = align.getLargestFaceBoundingBox(img)

        alignedFace = align.align_v1(imgDim, img, bb,
                                 landmarkIndices=openface.AlignDlib.OUTER_EYES_AND_NOSE)

        if alignedFace is not None:
            global current_box, current_name
            new_box = [bb.left(), bb.top(), bb.right(), bb.bottom()]

            refresh = True

            current_box = new_box
            current_name = "n/a"
            #return
            if current_name is not None and current_box is not None:
                if overlap(current_box, new_box):
                    pass
                   # refresh = False
            
            print("vectorize")
            if refresh:
                rep1 = net.forward(alignedFace)
                current_box = new_box
                #return
            else:
                current_box = new_box
                return
            print("complete")

            predict_num = clf.predict(rep1.reshape(1, 128))
            logits = clf.decision_function(rep1.reshape(1, 128))
            softmax = np.exp(logits) / np.sum(np.exp(logits))

            global col

            col = (255, 255, 0)

            if predict_num == len(num2name):
                name = "not recognized"
                col = (0, 0, 255)
            else:
                name = num2name[predict_num]

            confidence = int(np.max(softmax) * 100.0)

            name += ": %i%%" % confidence

            if confidence <= 90:
                col = (0, 128, 255)
            elif confidence <= 80:
                col = (0, 255, 255)

            current_name = name

            #print("face detected %s" % time.time())
        else:
            #print("face not detected %s" % time.time())
            current_box = None
            current_name = None

    if loop:
        while(True):
            if frame is None:
                continue

            detect()
    else:
        detect()

    #time.sleep(0.1)
    #frame = cv2.resize(np.hstack((frame, orig)), None, fx=2, fy=2, interpolation=cv2.INTER_LINEAR)

    # When everything done, release the capture


if __name__ == "__main__":
    mt = True

    if mt:
        thread.start_new_thread(start_display, (True,))
        #thread.start_new_thread(start_detection, (False,))

        while 1:
            pass
    else:
        start_display()
